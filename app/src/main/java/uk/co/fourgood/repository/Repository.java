package uk.co.fourgood.repository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import uk.co.fourgood.api.ApiService;
import uk.co.fourgood.api.model.IpAddress;
import uk.co.fourgood.util.netmanager.NetManager;

@Singleton
public class Repository {
    private ApiService apiService;
    @Inject
    NetManager netManager;
    @Inject
    public Repository(ApiService apiService) {
        this.apiService = apiService;
    }

    public Single<IpAddress> getDataFromApi(){
        if (netManager.isConnectedToInternet()) {
            return apiService.getJsonResponse();
        }
        return null;
    }

}
