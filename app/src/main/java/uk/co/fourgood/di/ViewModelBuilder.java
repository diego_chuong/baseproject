package uk.co.fourgood.di;

import androidx.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import uk.co.fourgood.util.di.ViewModelFactory;

@Module
abstract class ViewModelBuilder {
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory( ViewModelFactory factory );

}
