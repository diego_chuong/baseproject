package uk.co.fourgood.di;


import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import uk.co.fourgood.AndroidApp;

@Singleton
@Component(modules ={AndroidInjectionModule.class,AppModule.class, FragmentBuilder.class, ViewModelBuilder.class, ActivityBuilder.class})
public interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    public void inject(AndroidApp app);
}
