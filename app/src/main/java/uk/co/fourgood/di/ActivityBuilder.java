package uk.co.fourgood.di;

import androidx.lifecycle.ViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;
import uk.co.fourgood.ui.main.MainActivity;
import uk.co.fourgood.ui.main.MainActivityViewModel;

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector
    abstract MainActivity mainActivity();

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel.class)
    abstract ViewModel bindMainViewModel(MainActivityViewModel viewModel);
}
