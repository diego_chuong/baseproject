package uk.co.fourgood.di;


import androidx.lifecycle.ViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.multibindings.IntoMap;
import uk.co.fourgood.ui.main.MainActivityViewModel;
import uk.co.fourgood.ui.main.mainfragment.MainFragment;
import uk.co.fourgood.ui.main.mainfragment.MainFragmentViewModel;

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract MainFragment contributeMainFragment();
    @Binds
    @IntoMap
    @ViewModelKey(MainFragmentViewModel.class)
    abstract ViewModel bindMainViewModel(MainFragmentViewModel viewModel);

}