package uk.co.fourgood.di;

import android.app.Application;
import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.fourgood.api.ApiService;
import uk.co.fourgood.util.SchedulerProvider;

@Module
public class AppModule {

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return new SchedulerProvider(Schedulers.io(), AndroidSchedulers.mainThread());
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

    }
    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(Application application)  {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel( HttpLoggingInterceptor.Level.BASIC);

        File cacheDir = new File(application.getCacheDir(), UUID.randomUUID().toString());
        // 10 MiB cache
        Cache cache = new Cache(cacheDir, 10 * 1024 * 1024);

        return new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();
    }
//
    @Provides
    @Singleton
    ApiService provideApiService(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl("http://ip.jsontest.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build().create(ApiService.class);
    }


    @Provides
    @Singleton
    Context providerApplicationContext(Application application){
        return application.getApplicationContext();
    }

//
//    @Provides
//    @Singleton
//    PrefManager providerPrefManager(Context context,Gson gson){
//        return new PrefManager(context,gson);
//    }
//
//    @Provides
//    @Singleton
//    NetManager providerNetManager(Context context){
//        return new NetManager(context);
//    }
}
