package uk.co.fourgood.api;

import io.reactivex.Single;
import retrofit2.http.GET;
import uk.co.fourgood.api.model.IpAddress;

public interface ApiService {

    @GET(".")
    Single<IpAddress> getJsonResponse();
}
