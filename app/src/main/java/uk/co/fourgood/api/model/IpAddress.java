package uk.co.fourgood.api.model;

public class IpAddress {
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
