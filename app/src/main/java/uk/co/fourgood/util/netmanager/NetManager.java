package uk.co.fourgood.util.netmanager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import javax.inject.Inject;

public class NetManager {
    private Context applicationContext;
    @Inject
    public NetManager(Context applicationContext){
        this.applicationContext = applicationContext;
    }
    public boolean isConnectedToInternet()

    {

        ConnectivityManager conManager = (ConnectivityManager) applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = conManager.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
}
