package uk.co.fourgood.util;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

public class SchedulerProvider {

    private Scheduler backgroundScheduler;
    private Scheduler foregroundScheduler;

    public Scheduler getBackgroundScheduler() {
        return backgroundScheduler;
    }

    public void setBackgroundScheduler(Scheduler backgroundScheduler) {
        this.backgroundScheduler = backgroundScheduler;
    }

    public Scheduler getForegroundScheduler() {
        return foregroundScheduler;
    }

    public void setForegroundScheduler(Scheduler foregroundScheduler) {
        this.foregroundScheduler = foregroundScheduler;
    }
//

    public SchedulerProvider(Scheduler backgroundScheduler, Scheduler foregroundScheduler) {
        this.backgroundScheduler = backgroundScheduler;
        this.foregroundScheduler = foregroundScheduler;
    }

       public Observable  getSchedulersForObservable(Observable  observable)
    {
        return observable.subscribeOn(backgroundScheduler)
                .observeOn(foregroundScheduler);

    }


    public Single getSchedulersForSingle (Single single) {

        return single.subscribeOn(backgroundScheduler).observeOn(foregroundScheduler);
//            single.subscribeOn(backgroundScheduler)
//                    .observeOn(foregroundScheduler);
        }

    Completable getSchedulersForCompletable(Completable completable)  {
            return
                    completable.subscribeOn(backgroundScheduler)
                            .observeOn(foregroundScheduler);
        }


    Flowable getSchedulersForFlowable(Flowable flowable)  {
            return
            flowable.subscribeOn(backgroundScheduler)
                    .observeOn(foregroundScheduler);
        }
}
