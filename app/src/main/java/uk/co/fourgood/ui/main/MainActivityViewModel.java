package uk.co.fourgood.ui.main;

import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

import io.reactivex.Single;
import uk.co.fourgood.api.model.IpAddress;
import uk.co.fourgood.repository.Repository;
import uk.co.fourgood.util.SchedulerProvider;

public class MainActivityViewModel extends ViewModel {
    private Repository repository;
    private SchedulerProvider schedulerProvider;

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public SchedulerProvider getSchedulerProvider() {
        return schedulerProvider;
    }

    public void setSchedulerProvider(SchedulerProvider schedulerProvider) {
        this.schedulerProvider = schedulerProvider;
    }

    @Inject
    public MainActivityViewModel(Repository repository, SchedulerProvider schedulerProvider) {
        this.repository = repository;
        this.schedulerProvider = schedulerProvider;
    }

    //    SingleObserver<IpAddress> observer = new SingleObserver<IpAddress>() {
//        @Override
//        public void onSubscribe(Disposable d) {
//
//        }
//
//        @Override
//        public void onSuccess(IpAddress ipAddress) {
//
//        }
//
//        @Override
//        public void onError(Throwable e) {
//
//        }
//    };
    Single<IpAddress> showDataFromApi() {
        Single<IpAddress> ipAddressSingle = repository.getDataFromApi();
        return schedulerProvider.getSchedulersForSingle(ipAddressSingle);

    }
}
