package uk.co.fourgood.ui.main;

import android.util.Log;

import io.reactivex.observers.DisposableSingleObserver;
import uk.co.fourgood.R;
import uk.co.fourgood.api.model.IpAddress;
import uk.co.fourgood.databinding.ActivityMainBinding;
import uk.co.fourgood.ui.base.BaseActivity;

public class MainActivity extends BaseActivity<MainActivityViewModel, ActivityMainBinding> {

    @Override
    public void setUp() {
        compositeDisposable.add(viewModel.showDataFromApi().subscribeWith(new DisposableSingleObserver<IpAddress>() {
            @Override
            public void onSuccess(IpAddress ipAddress) {
                Log.e("TAG",ipAddress.getIp());
            }

            @Override
            public void onError(Throwable e) {
                Log.e("TAG",e.getMessage());
            }
        }));
         viewModel.showDataFromApi();
    }


    @Override
    public int setLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected Class<MainActivityViewModel> getViewModelType() {
        return MainActivityViewModel.class;
    }

    @Override
    public void setViewModel() {
        bindding.setViewModel(viewModel);
    }

}
