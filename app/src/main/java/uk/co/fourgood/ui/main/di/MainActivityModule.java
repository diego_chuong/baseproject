package uk.co.fourgood.ui.main.di;

import dagger.Module;
import dagger.Provides;
import uk.co.fourgood.repository.Repository;
import uk.co.fourgood.ui.main.MainActivityViewModel;
import uk.co.fourgood.util.SchedulerProvider;

@Module
public class MainActivityModule {


    @Provides
    MainActivityViewModel provideViewModel(Repository repository, SchedulerProvider schedulerProvider){
        return new MainActivityViewModel(repository, schedulerProvider);
    }
}
