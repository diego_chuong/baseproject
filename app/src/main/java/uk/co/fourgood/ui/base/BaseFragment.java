package uk.co.fourgood.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public abstract class BaseFragment<T extends ViewModel,V extends ViewDataBinding>  extends Fragment {

    protected V bindding;
    protected T viewModel ;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private final String KEY_SAMPLE= this.getClass().getSimpleName();

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bindding = DataBindingUtil.inflate(inflater,setLayoutId(), container, false);
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(getViewModelType());
        View rootView = bindding.getRoot();
        bindding.executePendingBindings();
        setUp();
        return rootView;

    }

    protected abstract void setUp();

    protected abstract Class<T> getViewModelType();

    protected abstract int setLayoutId();

}
