package uk.co.fourgood.ui.main.mainfragment;

import uk.co.fourgood.R;
import uk.co.fourgood.databinding.MainFragmentBinding;
import uk.co.fourgood.ui.base.BaseFragment;

public class MainFragment extends BaseFragment<MainFragmentViewModel, MainFragmentBinding> {
    @Override
    protected void setUp() {

    }

    @Override
    protected Class getViewModelType() {
        return MainFragmentViewModel.class;
    }

    @Override
    protected int setLayoutId() {
        return R.layout.main_fragment;
    }
}
