package uk.co.fourgood.ui.main.mainfragment.di;

import dagger.Module;
import dagger.Provides;
import uk.co.fourgood.ui.main.mainfragment.MainFragmentViewModel;
@Module
public class MainFragmentModule {
    @Provides
    MainFragmentViewModel provideViewModel(){
        return new MainFragmentViewModel();
    }
}
