package uk.co.fourgood.ui.base;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.disposables.CompositeDisposable;

@SuppressLint("Registered")
public abstract class BaseActivity<T extends ViewModel,V extends ViewDataBinding> extends AppCompatActivity implements HasSupportFragmentInjector {
    protected V bindding;
    protected CompositeDisposable compositeDisposable;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Inject ViewModelProvider.Factory viewModelFactory;
    protected T viewModel;


    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        compositeDisposable = new CompositeDisposable();
        bindding = DataBindingUtil.setContentView(this, setLayoutId());
        viewModel =ViewModelProviders.of(this, viewModelFactory)
                .get(getViewModelType());
        setViewModel();
        bindding.executePendingBindings();
        setUp();

    }


    public abstract void setUp();

    public abstract int setLayoutId();

    protected abstract Class<T> getViewModelType();

    public abstract void setViewModel();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
        compositeDisposable.dispose();
    }
}
